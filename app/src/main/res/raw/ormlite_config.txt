#
# generated on 2015/03/06 07:37:19
#
# --table-start--
dataClass=pl.wildeastcoders.dieta.database.model.Recipe
tableName=recipe
# --table-fields-start--
# --field-start--
fieldName=id
generatedId=true
# --field-end--
# --field-start--
fieldName=name
# --field-end--
# --field-start--
fieldName=type
# --field-end--
# --field-start--
fieldName=description
# --field-end--
# --field-start--
fieldName=used
# --field-end--
# --table-fields-end--
# --table-end--
#################################
# --table-start--
dataClass=pl.wildeastcoders.dieta.database.model.Ingredient
tableName=ingredient
# --table-fields-start--
# --field-start--
fieldName=id
generatedId=true
# --field-end--
# --field-start--
fieldName=name
# --field-end--
# --field-start--
fieldName=unit
# --field-end--
# --field-start--
fieldName=prevDay
# --field-end--
# --table-fields-end--
# --table-end--
#################################
# --table-start--
dataClass=pl.wildeastcoders.dieta.database.model.RecipeIngredient
tableName=recipeingredient
# --table-fields-start--
# --field-start--
fieldName=id
generatedId=true
# --field-end--
# --field-start--
fieldName=idRecipe
# --field-end--
# --field-start--
fieldName=idIngredient
# --field-end--
# --field-start--
fieldName=quantity
# --field-end--
# --table-fields-end--
# --table-end--
#################################
