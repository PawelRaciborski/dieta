package pl.wildeastcoders.dieta.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import pl.wildeastcoders.dieta.R;
import pl.wildeastcoders.dieta.database.model.Ingredient;

/**
 * Created by PR on 2015-01-29.
 */
public class IngredientSpinnerAdapter extends ArrayAdapter<Ingredient> {
    private final Context context;
    private final List<Ingredient> values;
//    HashMap<Ingredient, Integer> mIdMap = new HashMap<>();

    public IngredientSpinnerAdapter(Context context, int textViewResourceId,
                              List<Ingredient> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.values = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textView = (TextView)inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        textView.setText(this.values.get(position).getName());
        return textView;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textView = (TextView)inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        textView.setText(this.values.get(position).getName());
        return textView;
    }
}
