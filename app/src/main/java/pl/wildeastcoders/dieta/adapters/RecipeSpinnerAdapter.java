package pl.wildeastcoders.dieta.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import pl.wildeastcoders.dieta.database.model.Recipe;

/**
 * Created by PR on 2015-01-31.
 */
public class RecipeSpinnerAdapter extends ArrayAdapter<Recipe> {
    private final Context context;
    private final List<Recipe> values;

    public RecipeSpinnerAdapter(Context context, int textViewResourceId,
                                    List<Recipe> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.values = objects;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textView = (TextView)inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        textView.setText(this.values.get(position).getName());
        return textView;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textView = (TextView)inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        textView.setText(this.values.get(position).getName());
        return textView;
    }
}
