package pl.wildeastcoders.dieta.adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import pl.wildeastcoders.dieta.R;
import pl.wildeastcoders.dieta.database.model.Ingredient;

/**
 * Created by PR on 2015-01-31.
 */
public class IngredientsListAdapter extends ArrayAdapter<Ingredient> {

    private final Context context;
    private final List<Ingredient> values;

    public IngredientsListAdapter(Context context, int textViewResourceId,
                                  List<Ingredient> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.values = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final Ingredient ingredient = this.values.get(position);
        final View view = inflater.inflate(R.layout.listview_item_ingredient, parent, false);
        TextView label = (TextView) view.findViewById(R.id.label);
        CheckBox cbSelected = (CheckBox) view.findViewById(R.id.cb_selected);
        final EditText etIngredientQuantity = (EditText) view.findViewById(R.id.et_ingredient_quantity);
        label.setText(ingredient.getName());
        etIngredientQuantity.setEnabled(ingredient.isPicked());
        etIngredientQuantity.setText(Integer.toString(ingredient.getRecipeIngredientQuantity()));
        cbSelected.setChecked(ingredient.isPicked());
        cbSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (ingredient.getRecipeIngredientQuantity() <= 0) {
                        ingredient.setRecipeIngredientQuantity(0);
                        etIngredientQuantity.setText("");
                    }
                }
                etIngredientQuantity.setEnabled(isChecked);
                ingredient.setPicked(isChecked);

            }
        });
        etIngredientQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int q = 0;
                try {
                    q = Integer.parseInt(s.toString());
                    ingredient.getRecipeIngredient().setModified(q>0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ingredient.setRecipeIngredientQuantity(q);
            }
        });


//        NumberPicker np = (NumberPicker) view.findViewById(R.id.np_ingredient_quantity);
//        np.setMaxValue(9);
//        np.setMinValue(0);
        return view;
    }


//    @Override
//    public View getDropDownView(int position, View convertView, ViewGroup parent) {
//        LayoutInflater inflater = (LayoutInflater) context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        TextView textView = (TextView)inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
//        textView.setText(this.values.get(position).getName());
//        return textView;
//    }

}
