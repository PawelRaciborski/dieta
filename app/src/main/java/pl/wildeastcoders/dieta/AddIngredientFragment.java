package pl.wildeastcoders.dieta;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import pl.wildeastcoders.dieta.adapters.IngredientSpinnerAdapter;
import pl.wildeastcoders.dieta.database.DatabaseHelper;
import pl.wildeastcoders.dieta.database.model.Ingredient;


public class AddIngredientFragment extends BaseDbFragment {

    private Spinner spinnerIngredients;
    private Spinner spinnerUnits;
    private Spinner spinnerWhenBuy;
    private Button btnSave;
    private TextView tvIngredientName;
    private Button btnAdd;
    private Button btnDelete;
    private IngredientSpinnerAdapter ingredientSpinnerAdapter;



    private List<Ingredient> ingredients;

    private Ingredient nullIngredient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.nullIngredient = new Ingredient("Dodaj składnik", 0, 0);
        this.nullIngredient.setId(-1);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_ingredient, container, false);

        loadIngredientsList();

        spinnerIngredients = (Spinner) view.findViewById(R.id.spinner_ingredients);
        ingredientSpinnerAdapter = new IngredientSpinnerAdapter(getActivity(), android.R.layout.simple_list_item_1, this.ingredients);
        spinnerIngredients.setAdapter(ingredientSpinnerAdapter);

        spinnerIngredients.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Ingredient i = ingredients.get(position);
                if (i != null && i.getId() != -1) {
                    tvIngredientName.setText(i.getName());
                    spinnerUnits.setSelection(i.getUnit());
                    spinnerWhenBuy.setSelection(i.getPrevDay());
                } else {
                    tvIngredientName.setText("");
                    spinnerUnits.setSelection(0);
                    spinnerWhenBuy.setSelection(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerUnits = (Spinner) view.findViewById(R.id.spinner_unit);
        ArrayAdapter<CharSequence> adapterUnits = ArrayAdapter.createFromResource(this.getActivity(),
                R.array.units, android.R.layout.simple_spinner_item);
        adapterUnits.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerUnits.setAdapter(adapterUnits);

        spinnerWhenBuy = (Spinner) view.findViewById(R.id.spinner_when_buy);
        ArrayAdapter<CharSequence> adapterWhenBuy = ArrayAdapter.createFromResource(this.getActivity(),
                R.array.when_buy, android.R.layout.simple_spinner_item);
        adapterWhenBuy.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerWhenBuy.setAdapter(adapterWhenBuy);

        tvIngredientName = (TextView) view.findViewById(R.id.tv_ingredient_name);

        btnAdd = (Button) view.findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = tvIngredientName.getText().toString();
                int whenBuy = spinnerWhenBuy.getSelectedItemPosition();
                int unit = spinnerUnits.getSelectedItemPosition();

                Ingredient i = new Ingredient(name, unit, whenBuy);
                try {
                    dbHelper.getIngredientDao().create(i);
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                loadIngredientsList();

            }
        });

        btnSave = (Button) view.findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spinnerIngredients.getSelectedItemPosition() == 0) {
                    return;
                }
                Ingredient i = (Ingredient) spinnerIngredients.getSelectedItem();
                if (i != null && i != nullIngredient) {
                    i.setName(tvIngredientName.getText().toString());
                    i.setPrevDay(spinnerWhenBuy.getSelectedItemPosition());
                    i.setUnit(spinnerUnits.getSelectedItemPosition());
                    try {
                        dbHelper.getIngredientDao().createOrUpdate(i);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    loadIngredientsList();
                }
            }
        });

        btnDelete = (Button) view.findViewById(R.id.btn_delete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spinnerIngredients.getSelectedItemPosition() == 0) {
                    return;
                }
                Ingredient i = (Ingredient) spinnerIngredients.getSelectedItem();
                if (i != null && i != nullIngredient) {
                    try {
                        dbHelper.getIngredientDao().delete(i);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    loadIngredientsList();
                    spinnerIngredients.setSelection(0);
                }
            }
        });

        return view;
    }

    private void loadIngredientsList() {
        try {
            if (this.ingredients == null) {
                this.ingredients = new LinkedList<>();
            } else {
                this.ingredients.clear();
            }
            this.ingredients.add(this.nullIngredient);
            this.ingredients.addAll(dbHelper.getIngredientDao().queryForAll());
        } catch (SQLException e) {
            this.ingredients = new LinkedList<>();
            e.printStackTrace();
        }
        if (this.ingredientSpinnerAdapter != null) {
            this.ingredientSpinnerAdapter.notifyDataSetChanged();
        }
    }
}
