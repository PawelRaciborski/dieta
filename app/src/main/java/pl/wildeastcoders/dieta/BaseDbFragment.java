package pl.wildeastcoders.dieta;

import android.support.v4.app.Fragment;

import pl.wildeastcoders.dieta.database.DatabaseHelper;

/**
 * Created by PR on 2015-01-31.
 */
public abstract class BaseDbFragment extends Fragment {
    public void setDbHelper(DatabaseHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    protected DatabaseHelper dbHelper;
}
