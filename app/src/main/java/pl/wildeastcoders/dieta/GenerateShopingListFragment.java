package pl.wildeastcoders.dieta;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import pl.wildeastcoders.dieta.R;
import pl.wildeastcoders.dieta.database.model.FoodDayFactory;
import pl.wildeastcoders.dieta.database.model.Ingredient;
import pl.wildeastcoders.dieta.database.model.Recipe;
import pl.wildeastcoders.dieta.database.model.RecipeIngredient;

/**
 * A simple {@link Fragment} subclass.
 */
public class GenerateShopingListFragment extends BaseDbFragment {

    private FoodDayFactory factory;

    public GenerateShopingListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.factory = new FoodDayFactory(this.dbHelper, 7,2,2);

        return inflater.inflate(R.layout.fragment_generate_shoping_list, container, false);
    }








}
