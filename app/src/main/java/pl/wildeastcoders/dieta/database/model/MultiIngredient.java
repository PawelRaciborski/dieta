package pl.wildeastcoders.dieta.database.model;

import android.content.Context;

import java.util.LinkedList;

import pl.wildeastcoders.dieta.R;
import pl.wildeastcoders.dieta.exceptions.EmptyMultiIngredientException;
import pl.wildeastcoders.dieta.exceptions.InvelidIngredientException;

/**
 * Created by PR on 2015-02-26.
 */
public class MultiIngredient {
    private LinkedList<Ingredient> ingredients;
    private double portion = 1.0f;


    public MultiIngredient() {
        ingredients = new LinkedList<>();
    }

    public MultiIngredient(Ingredient ingredient) {
        ingredients = new LinkedList<>();
        ingredients.add(ingredient);
    }

    public double getPortion() {
        return portion;
    }

    public void setPortion(double portion) {
        this.portion = portion;
    }

    public void add(Ingredient ingredient) throws InvelidIngredientException, EmptyMultiIngredientException {
        if (ingredients.size() > 0) {
            if (getFirst().getId() != ingredient.getId()) {
                throw new InvelidIngredientException();
            }
        }
        ingredients.add(ingredient);
    }

    public void add(MultiIngredient multiIngredient) throws EmptyMultiIngredientException, InvelidIngredientException {
        if (ingredients.size() > 0) {
            if (getFirst().getId() != multiIngredient.getFirst().getId()) {
                throw new InvelidIngredientException();
            }
        }
        ingredients.addAll(multiIngredient.ingredients);
    }

    private Ingredient getFirst() throws EmptyMultiIngredientException {
        if (ingredients.size() <= 0) {
            throw new EmptyMultiIngredientException();
        }
        return ingredients.getFirst();
    }

//    public int getUnit() throws EmptyMultiIngredientException{
//        return getFirst().getUnit();
//    }

    public int getQuantity() {
        int sum = 0;
        for (Ingredient ingredient : ingredients) {
            sum += ingredient.getRecipeIngredientQuantity();
        }
        return (int) Math.ceil(sum * portion);
    }


    public String readIngredientString(Context context) {
        try {
            Ingredient ingredient = getFirst();
            StringBuilder sb = new StringBuilder();
            sb.append(ingredient.getName()).append(" - ")
                    .append(getQuantity()).append(" ")
                    .append(context.getResources().getStringArray(R.array.units)[ingredient.getUnit()]).append("\n");
            return sb.toString();
        } catch (EmptyMultiIngredientException e) {
            e.printStackTrace();
            return "";
        }
    }
}
