package pl.wildeastcoders.dieta.database.model;

import com.j256.ormlite.field.DatabaseField;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by PR on 2015-01-25.
 */


public class Recipe {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String name;

    @DatabaseField
    private int type;

    @DatabaseField
    private String description;

    @DatabaseField
    private boolean used;


    private List<Ingredient> ingredientList;

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String TYPE = "type";
    public static final String DESCRIPTION = "description";
    public static final String USED = "used";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<Ingredient> getIngredientList() {
        return ingredientList;
    }

    public void setIngredientList(List<Ingredient> ingredientList) {
        this.ingredientList = ingredientList;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public Recipe() {
    }

    public Recipe(String name, String description, int type) {
        this.name = name;
        this.description = description;
        this.type = type;
        this.used = false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("RECIPE [id= ").append(id).append(", subject= ").append(name).append(", text= ").append(description).append(", type= ").append(type).append(", used= ").append(used).append("]");
        return sb.toString();
    }

    public void addIngredient(Ingredient ingredient){
        if(this.ingredientList==null){
            this.ingredientList = new LinkedList<>();
        }
        this.ingredientList.add(ingredient);
    }

    public LinkedList<Ingredient> getIngredientsList(int typeCode){
        LinkedList<Ingredient> output = new LinkedList<>();
        for(Ingredient ingredient : ingredientList){
            if(ingredient.getPrevDay() == typeCode){
                output.add(ingredient);
            }
        }
        return output;
    }


}
