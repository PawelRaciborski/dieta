package pl.wildeastcoders.dieta.database.model;

import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import pl.wildeastcoders.dieta.database.DatabaseHelper;

/**
 * Created by PR on 2015-02-13.
 */
public class FoodDayFactory {

    private DatabaseHelper dbHelper;



//    private List<Recipe> notusedFirstBreakfastList;
//    private List<Recipe> notusedSecondBreakfastList;
//    private List<Recipe> notusedThirdBreakfastList;
//    private List<Recipe> notusedDinnerList;
//
//    private List<Recipe> usedFirstBreakfastList;
//    private List<Recipe> usedSecondBreakfastList;
//    private List<Recipe> usedThirdBreakfastList;
//    private List<Recipe> usedDinnerList;

    private RecipeManager firstBreakfasRecipeManager;
    private RecipeManager secondBreakfastRecipeManager;
    private RecipeManager thirdBreakfastListRecipeManager;
    private RecipeManager dinnerRecipeManager;

    private int days;
    private int duration;
    private double portions;
    private int[] daysPerRecipe;


    public FoodDayFactory(DatabaseHelper dbHelper, int days, int duration, double portions) {
        this.dbHelper = dbHelper;
        this.days = days;
        this.duration = duration;
        this.portions = portions;
        //this.daysPerRecipe = calculateDaysPerRecipe(this.days, this.duration);
        loadRecipes();

        List<FoodDay> fd = produceFoodDayList();
    }

//    private static int[] calculateDaysPerRecipe(int days, int duration) {
//
//        int len = (int) Math.ceil(days / (double) duration);
//        if (len <= 0) {
//            return null;
//        }
//
//        int[] result = new int[len];
//
//        for (int i = 0; i < len; i++) {
//            if ((i + 1) * duration <= days) {
//                result[i] = duration;
//            } else if ((i + 1) * duration - days > 0) {
//                result[i] = (i + 1) * duration - days;
//            }
//        }
//        return result;
//
//    }

    public DatabaseHelper getDbHelper() {
        return dbHelper;
    }

    public void setDbHelper(DatabaseHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    private void loadRecipes() {
        try {
            List<Recipe> firstBreakfastList;
            List<Recipe> secondBreakfastList;
            List<Recipe> thirdBreakfastList;
            List<Recipe> dinnerList;
            firstBreakfastList = dbHelper.getRecipeDao().queryForEq(Recipe.TYPE, 0);
            secondBreakfastList = dbHelper.getRecipeDao().queryForEq(Recipe.TYPE, 1);
            thirdBreakfastList = dbHelper.getRecipeDao().queryForEq(Recipe.TYPE, 2);
            dinnerList = dbHelper.getRecipeDao().queryForEq(Recipe.TYPE, 3);

            completeRecipeList(firstBreakfastList);
            completeRecipeList(secondBreakfastList);
            completeRecipeList(thirdBreakfastList);
            completeRecipeList(dinnerList);

            firstBreakfasRecipeManager = new RecipeManager(firstBreakfastList);
            secondBreakfastRecipeManager = new RecipeManager(secondBreakfastList);
            thirdBreakfastListRecipeManager = new RecipeManager(thirdBreakfastList);
            dinnerRecipeManager = new RecipeManager(dinnerList);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void loadRecipeIngredient(Recipe recipe) throws SQLException {
        List<RecipeIngredient> recipeIngredientList = dbHelper.getRecipeIngredientDao().queryForEq(RecipeIngredient.ID_RECIPE, recipe.getId());
        for (RecipeIngredient r : recipeIngredientList) {
            List<Ingredient> list = dbHelper.getIngredientDao().queryForEq(Ingredient.ID, r.getIdIngredient());
            if (list.size() == 1 && list.get(0) != null) {
                Ingredient ingredient = list.get(0);
                ingredient.setRecipeIngredient(r);
                recipe.addIngredient(ingredient);
            }
        }
    }

    private void completeRecipeList(List<Recipe> recipeList) throws SQLException {
        for (Recipe r : recipeList) {
            loadRecipeIngredient(r);
        }
    }

    public FoodDay produceFoodDay() {

        FoodDay fd = new FoodDay(firstBreakfasRecipeManager.getNextRecipe(),
                secondBreakfastRecipeManager.getNextRecipe(),
                thirdBreakfastListRecipeManager.getNextRecipe(),
                dinnerRecipeManager.getNextRecipe());
        fd.setPortion(this.portions);
        return fd;
    }

    public List<FoodDay> produceFoodDayList() {
        LinkedList<FoodDay> list = new LinkedList<>();
        FoodDay currentFoodDay = null;

        for (int i = 0; i < this.days; i++) {
            if (i % duration == 0) {
                currentFoodDay = produceFoodDay();
                list.add(currentFoodDay);
            }else{
                currentFoodDay.incrementDuration();
            }
        }
        return list;

    }

    public void commitRecipes(){
        try {
            LinkedList<Recipe> allRecipes = new LinkedList<>();
            allRecipes.addAll(firstBreakfasRecipeManager.getAllRecipes());
            allRecipes.addAll(secondBreakfastRecipeManager.getAllRecipes());
            allRecipes.addAll(thirdBreakfastListRecipeManager.getAllRecipes());
            allRecipes.addAll(dinnerRecipeManager.getAllRecipes());

            for(Recipe r : allRecipes){
                dbHelper.getRecipeDao().update(r);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private class RecipeManager{

        private List<Recipe> baseList;
        private List<Recipe> usedList;
        private List<Recipe> notUsedList;


        public RecipeManager(List<Recipe> inputList){
            baseList = inputList;
            usedList = readUsed(inputList);
            notUsedList = readUnused(inputList);
            for (Recipe r : baseList){
                r.setUsed(false);
            }
        }

        private LinkedList<Recipe> readUnused(List<Recipe> inputList){
            return readRecipeFromList(inputList, false);

        }

        private LinkedList<Recipe> readUsed(List<Recipe> inputList){
            return readRecipeFromList(inputList, true);
        }

        private LinkedList<Recipe> readRecipeFromList(List<Recipe> inputList, boolean isUsed){
            LinkedList<Recipe> outputList = new LinkedList<>();
            for(Recipe rec : inputList){
                if(rec.isUsed() == isUsed){
                    outputList.add(rec);
                }
            }
            return outputList;
        }

        public Recipe getNextRecipe(){
            if (notUsedList.size() <= 0) {
                notUsedList.clear();
                usedList.clear();
                notUsedList.addAll(baseList);
            }
            Random random = new Random(Calendar.getInstance().getTime().getTime());
            int randomValue = random.nextInt(notUsedList.size());
            Recipe r = notUsedList.get(randomValue);
            r.setUsed(true);
            notUsedList.remove(randomValue);
            usedList.add(r);
            return r;
        }

        public LinkedList<Recipe> getAllRecipes(){
            LinkedList<Recipe> ret = new LinkedList<>(usedList);
            ret.addAll(notUsedList);
            return ret;
        }
    }

}
