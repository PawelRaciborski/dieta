package pl.wildeastcoders.dieta.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import pl.wildeastcoders.dieta.R;
import pl.wildeastcoders.dieta.database.model.Ingredient;
import pl.wildeastcoders.dieta.database.model.Recipe;
import pl.wildeastcoders.dieta.database.model.RecipeIngredient;

/**
 * Created by PR on 2015-01-25.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "diet.db";
    private static final int DATABASE_VERSION = 1;

    private Dao<Recipe, Integer> recipeDao = null;
    private RuntimeExceptionDao<Recipe, Integer> recipeRuntimeExceptionDao = null;

    private Dao<RecipeIngredient, Integer> recipeIngredientDao = null;
    private RuntimeExceptionDao<RecipeIngredient, Integer> recipeIngredientRuntimeExceptionDao = null;

    private Dao<Ingredient, Integer> ingredientDao = null;
    private RuntimeExceptionDao<Ingredient, Integer> ingredientRuntimeExceptionDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Recipe.class);
            TableUtils.createTable(connectionSource, Ingredient.class);
            TableUtils.createTable(connectionSource, RecipeIngredient.class);

            for(int i=0;i<12;i++){
                Ingredient ingredient = new Ingredient("Ingredient " + i, i%3, i%2);
                getIngredientDao().create(ingredient);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Recipe.class, true);
            TableUtils.dropTable(connectionSource, Ingredient.class, true);
            TableUtils.dropTable(connectionSource, RecipeIngredient.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Dao<Recipe, Integer> getRecipeDao() throws SQLException {

        if (recipeDao == null) {
            recipeDao = getDao(Recipe.class);
        }

        return recipeDao;
    }

    public RuntimeExceptionDao<Recipe, Integer> getRecipeRuntimeExceptionDao() {
        if (recipeRuntimeExceptionDao == null) {
            recipeRuntimeExceptionDao = getRuntimeExceptionDao(Recipe.class);
        }
        return recipeRuntimeExceptionDao;
    }

    public Dao<Ingredient, Integer> getIngredientDao() throws SQLException {

        if (ingredientDao == null) {
            ingredientDao = getDao(Ingredient.class);
        }

        return ingredientDao;
    }

    public RuntimeExceptionDao<Ingredient, Integer> getIngredientRuntimeExceptionDao() {
        if (ingredientRuntimeExceptionDao == null) {
            ingredientRuntimeExceptionDao = getRuntimeExceptionDao(Ingredient.class);
        }
        return ingredientRuntimeExceptionDao;
    }

    public Dao<RecipeIngredient, Integer> getRecipeIngredientDao() throws SQLException {

        if (recipeIngredientDao == null) {
            recipeIngredientDao = getDao(RecipeIngredient.class);
        }

        return recipeIngredientDao;
    }

    public RuntimeExceptionDao<RecipeIngredient, Integer> getRecipeIngredientRuntimeExceptionDao() {
        if (recipeIngredientRuntimeExceptionDao == null) {
            recipeIngredientRuntimeExceptionDao = getRuntimeExceptionDao(RecipeIngredient.class);
        }
        return recipeIngredientRuntimeExceptionDao;
    }
}
