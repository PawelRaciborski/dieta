package pl.wildeastcoders.dieta.database.model;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import pl.wildeastcoders.dieta.R;
import pl.wildeastcoders.dieta.exceptions.EmptyMultiIngredientException;
import pl.wildeastcoders.dieta.exceptions.InvelidIngredientException;
import pl.wildeastcoders.dieta.utils.ShopingListGenerator;

/**
 * Created by PR on 2015-02-13.
 */
public class FoodDay {
    private Recipe firstBreakfest;
    private Recipe secondBreakfest;
    private Recipe thirdBreakfest;
    private Recipe dinner;

    private double portion = 1.0f;

    private int duration = 1;

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public FoodDay(Recipe firstBreakfest, Recipe secondBreakfest, Recipe thirdBreakfest, Recipe dinner) {
        this.firstBreakfest = firstBreakfest;
        this.secondBreakfest = secondBreakfest;
        this.thirdBreakfest = thirdBreakfest;
        this.dinner = dinner;
    }

    public double getPortion() {
        return portion;
    }

    public void setPortion(double portion) {
        this.portion = portion;
    }

    public Recipe getSecondBreakfest() {
        return secondBreakfest;
    }

    public void setSecondBreakfest(Recipe secondBreakfest) {
        this.secondBreakfest = secondBreakfest;
    }

    public Recipe getThirdBreakfest() {
        return thirdBreakfest;
    }

    public void setThirdBreakfest(Recipe thirdBreakfest) {
        this.thirdBreakfest = thirdBreakfest;
    }

    public Recipe getDinner() {
        return dinner;
    }

    public void setDinner(Recipe dinner) {
        this.dinner = dinner;
    }

    public Recipe getFirstBreakfest() {
        return firstBreakfest;
    }

    public void setFirstBreakfest(Recipe firstBreakfest) {
        this.firstBreakfest = firstBreakfest;
    }


    public List<MultiIngredient> getDayIngredients() {
        HashMap<Integer, MultiIngredient> uniqueIngredients = getUniqueIngredients(Ingredient.PREVIOUS_DAY);
        List<MultiIngredient> dayIngredients = new ArrayList<MultiIngredient>(uniqueIngredients.values());
        return dayIngredients;
    }

    public HashMap<Integer, MultiIngredient> getWeekIngredients() {
        return getUniqueIngredients(Ingredient.ONCE_A_WEEK);
    }

    private HashMap<Integer, MultiIngredient> getUniqueIngredients(int typeCode) {
        LinkedList<Ingredient> dayIngredients = new LinkedList<>();
        dayIngredients.addAll(firstBreakfest.getIngredientsList(typeCode));
        dayIngredients.addAll(secondBreakfest.getIngredientsList(typeCode));
        dayIngredients.addAll(thirdBreakfest.getIngredientsList(typeCode));
        dayIngredients.addAll(dinner.getIngredientsList(typeCode));

        HashMap<Integer, MultiIngredient> uniqueIngredients = new HashMap<>();

        for (Ingredient ingredient : dayIngredients) {
            if (uniqueIngredients.containsKey(ingredient.getId())) {
                try {
                    uniqueIngredients.get(ingredient.getId()).add(ingredient);
                } catch (InvelidIngredientException e) {
                    e.printStackTrace();
                } catch (EmptyMultiIngredientException e) {
                    e.printStackTrace();
                }
            } else {
                MultiIngredient multiIngredient = new MultiIngredient(ingredient);
                multiIngredient.setPortion(portion * duration);
                uniqueIngredients.put(ingredient.getId(), multiIngredient);
            }
        }
        return uniqueIngredients;
    }

    public String getDayDescription(Context context){

        String [] recipeTypes = context.getResources().getStringArray(R.array.recipe_type);

        StringBuilder stringBuilder = new StringBuilder();

        appendRecipe(firstBreakfest, stringBuilder, recipeTypes);
        appendRecipe(secondBreakfest, stringBuilder, recipeTypes);
        appendRecipe(thirdBreakfest, stringBuilder, recipeTypes);
        appendRecipe(dinner, stringBuilder, recipeTypes);

        stringBuilder.append(ShopingListGenerator.multiIngredientsToString(getDayIngredients(),context));

        return stringBuilder.toString();
    }

    private void appendRecipe(Recipe recipe, StringBuilder stringBuilder, String [] recipeTypes){
        stringBuilder.append(recipeTypes[recipe.getType()]).append(": ").append(recipe.getName()).append("\n\n");
        stringBuilder.append(recipe.getDescription()).append("\n\n");
    }

    public void incrementDuration() {
        this.duration++;
    }
}
