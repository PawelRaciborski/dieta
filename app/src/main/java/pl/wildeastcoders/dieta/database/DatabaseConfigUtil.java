package pl.wildeastcoders.dieta.database;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import java.io.IOException;
import java.sql.SQLException;

import pl.wildeastcoders.dieta.database.model.Ingredient;
import pl.wildeastcoders.dieta.database.model.Recipe;
import pl.wildeastcoders.dieta.database.model.RecipeIngredient;

/**
 * Created by PR on 2015-01-25.
 */
public class DatabaseConfigUtil extends OrmLiteConfigUtil{

    private static final Class<?>[] classes = new Class[]{Recipe.class, Ingredient.class, RecipeIngredient.class};

    public static void main(String [] args)throws SQLException,IOException{
        writeConfigFile("ormlite_config.txt",classes);
    }

}
