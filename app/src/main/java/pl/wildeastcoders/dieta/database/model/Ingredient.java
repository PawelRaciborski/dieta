package pl.wildeastcoders.dieta.database.model;

import android.content.Context;

import com.j256.ormlite.field.DatabaseField;

import pl.wildeastcoders.dieta.R;

/**
 * Created by PR on 2015-01-25.
 */
public class Ingredient {

    /**
     * DB fields.
     */
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private String name;
    @DatabaseField
    private int unit;
    @DatabaseField
    private int prevDay;

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String UNIT = "unit";
    public static final String PREV_DAY = "prevDay";

    public static final int ONCE_A_WEEK = 0;
    public static final int PREVIOUS_DAY = 1;

    public static final int UNIT_GRAM = 0;
    public static final int UNIT_ITEM = 1;
    public static final int UNIT_MILILITER = 2;

    /**
     * Additional fields.
     */

    private boolean isPicked = false;
    private RecipeIngredient recipeIngredient;

    public Ingredient() {
    }

    public Ingredient(String name, int unit, int prevDay) {
        this.name = name;
        this.unit = unit;
        this.prevDay = prevDay;
    }

    public Ingredient(String name, int unit, int prevDay, RecipeIngredient recipeIngredient) {
        this.name = name;
        this.unit = unit;
        this.prevDay = prevDay;
        this.recipeIngredient = recipeIngredient;
        //if(recipeIngredient!=null){
        this.isPicked = (recipeIngredient != null && recipeIngredient.getIdRecipe() > 0);
        //}
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public int getPrevDay() {
        return prevDay;
    }

    public void setPrevDay(int prevDay) {
        this.prevDay = prevDay;
    }

    public RecipeIngredient getRecipeIngredient() {
        return recipeIngredient;
    }

    public void setRecipeIngredient(RecipeIngredient recipeIngredient) {
        this.isPicked = (recipeIngredient != null && recipeIngredient.getIdRecipe() > 0);
        this.recipeIngredient = recipeIngredient;
    }

    public boolean isPicked() {
        return isPicked;
    }

    public void setPicked(boolean isPicked) {
        this.isPicked = isPicked;
    }

    /**
     * Metody dostepowe do RecipeIngredient
     */

    public int getRecipeIngredientQuantity() {
        if (this.recipeIngredient != null) {
            return this.recipeIngredient.getQuantity();
        }
        return 0;
    }

    public boolean setRecipeIngredientQuantity(int quantity) {
        if (this.recipeIngredient == null) {
            this.recipeIngredient = new RecipeIngredient(-1, this.getId(), quantity);
            this.recipeIngredient.setDirty(true);
            return false;
        } else {
            this.recipeIngredient.setQuantity(quantity);
            return true;
        }

    }

    public String getUnitString(Context context) {
        return context.getResources().getStringArray(R.array.units)[this.unit];
    }

    public String getIngredientDescription(Context c) {
        StringBuilder sb = new StringBuilder();
        sb.append(name).append(", ");
        sb.append(getRecipeIngredient().getQuantity()).append(" ").append(getUnitString(c));
        return sb.toString();
    }

    public void setDirty(boolean isDirty){
        if(this.recipeIngredient!=null){
            this.recipeIngredient.setDirty(isDirty);
        }
    }

    public boolean isDirty(){
        if(this.recipeIngredient!=null){
            return this.recipeIngredient.isDirty();
        }else{
            return true;
        }
    }

}
