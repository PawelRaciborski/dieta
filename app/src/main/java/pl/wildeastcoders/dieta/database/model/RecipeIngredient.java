package pl.wildeastcoders.dieta.database.model;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by PR on 2015-01-25.
 */
public class RecipeIngredient {


    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private int idRecipe;
    @DatabaseField
    private int idIngredient;
    @DatabaseField
    private int quantity;


    public static final String ID = "id";
    public static final String ID_RECIPE = "idRecipe";
    public static final String ID_INGREDIENT = "idIngredient";
    public static final String QUANTITY = "quantity";

    private boolean isDirty = false;

    private boolean isModified = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdRecipe() {
        return idRecipe;
    }

    public void setIdRecipe(int idRecipe) {
        this.idRecipe = idRecipe;
    }

    public int getIdIngredient() {
        return idIngredient;
    }

    public void setIdIngredient(int idIngredient) {
        this.idIngredient = idIngredient;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isDirty() {
        return isDirty;
    }

    public void setDirty(boolean isDirty) {
        this.isDirty = isDirty;
    }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean isModified) {
        this.isModified = isModified;
    }

    public RecipeIngredient() {
    }

    public RecipeIngredient(int idRecipe, int idIngredient, int quantity) {
        this.idRecipe = idRecipe;
        this.idIngredient = idIngredient;
        this.quantity = quantity;
    }
}
