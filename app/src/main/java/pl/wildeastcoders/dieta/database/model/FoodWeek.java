package pl.wildeastcoders.dieta.database.model;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pl.wildeastcoders.dieta.exceptions.EmptyMultiIngredientException;
import pl.wildeastcoders.dieta.exceptions.InvelidIngredientException;
import pl.wildeastcoders.dieta.utils.ShopingListGenerator;

/**
 * Created by PR on 2015-02-26.
 */
public class FoodWeek {
    private int foodDayCount;
    private ArrayList<FoodDay> foodDays;
    private FoodDayFactory foodDayFactory;

    public FoodWeek(int foodDayCount, FoodDayFactory foodDayFactory) {
        this.foodDayCount = foodDayCount;
        this.foodDayFactory = foodDayFactory;
        foodDays = new ArrayList<>(foodDayCount);
    }

    public void init() {
        for (int i = 0; i < foodDayCount; i++) {
            FoodDay next = foodDayFactory.produceFoodDay();
            foodDays.add(next);
        }
    }

    private List<MultiIngredient> getWeekIngredientsList() {
        HashMap<Integer, MultiIngredient> output = new HashMap<>();
        for (FoodDay fd : foodDays) {
            HashMap<Integer, MultiIngredient> tmp = fd.getWeekIngredients();
            for (Integer k : tmp.keySet()) {
                if (output.containsKey(k)) {
                    try {
                        output.get(k).add(tmp.get(k));
                    } catch (EmptyMultiIngredientException e) {
                        e.printStackTrace();
                    } catch (InvelidIngredientException e) {
                        e.printStackTrace();
                    }
                } else {
                    tmp.put(k, tmp.get(k));
                }
            }
        }
        List<MultiIngredient> weekIngredients = new ArrayList<MultiIngredient>(output.values());
        return weekIngredients;
    }

    public String getWeekDescription(Context context) {
        StringBuilder stringBuilder = new StringBuilder();

        FoodDay fd;
        for (int i = 0; i < foodDays.size(); i++) {
            fd = foodDays.get(i);
            stringBuilder.append("Dni [").append((i + 1)).append(" - ").append(i + 1 + fd.getDuration()).append("]:\n\n");
            i += fd.getDuration();
            stringBuilder.append(fd.getDayDescription(context));
        }
        stringBuilder.append("Na cały tydzień:\n\n");
        stringBuilder.append(ShopingListGenerator.multiIngredientsToString(getWeekIngredientsList(), context));

        return stringBuilder.toString();
    }
}
