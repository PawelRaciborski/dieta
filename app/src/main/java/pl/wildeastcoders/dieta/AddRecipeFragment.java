package pl.wildeastcoders.dieta;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import pl.wildeastcoders.dieta.adapters.IngredientsListAdapter;
import pl.wildeastcoders.dieta.adapters.RecipeSpinnerAdapter;
import pl.wildeastcoders.dieta.database.model.Ingredient;
import pl.wildeastcoders.dieta.database.model.Recipe;
import pl.wildeastcoders.dieta.database.model.RecipeIngredient;
import pl.wildeastcoders.dieta.dialogs.IngredientsDialogFragment;
import pl.wildeastcoders.dieta.utils.CroutonValidationFailedRenderer;
import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.Form;
import ua.org.zasadnyy.zvalidations.ValidationFailedRenderer;
import ua.org.zasadnyy.zvalidations.validations.NotEmpty;

public class AddRecipeFragment extends BaseDbFragment implements IngredientsDialogFragment.IngredientsModificationListener {

    private Spinner spinnerRecipes;
    private EditText etRecipeName;
    private EditText etRecipeDescription;
    private TextView tvIngredientsDescription;
    private Button btnPickIngredients;
    private Spinner spinnerType;

    private RecipeSpinnerAdapter recipesSpinnerAdapter;
    private IngredientsListAdapter ingredientsListAdapter;

    private Button btnAdd;
    private Button btnSave;
    private Button btnDelete;

    private List<Ingredient> ingredients;
    private List<Recipe> recipes;
    private List<RecipeIngredient> recipeIngredientList;
    private Recipe nullRecipe;
    private Form mForm;

    private static final int MODE_SAVE_ADD = 0;
    private static final int MODE_SAVE_UPDATE = 1;

    private Map<Integer, ValidationFailedRenderer> mVaildationRenderers;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.nullRecipe = new Recipe("Dodaj przepis", "", 0);
        this.nullRecipe.setId(-1);
        if (getArguments() != null) {
        }
    }

    private int getRecipeIndex(Recipe r){
        for(int i = 0;i< this.recipes.size();i++){
            if(this.recipes.get(i).getId() == r.getId()){
                return i;
            }
        }
        return 0;
    }

    private void setSpinnerAtRecipe(Recipe r){
        spinnerRecipes.setSelection(getRecipeIndex(r));
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_recipe, container, false);


        loadRecipesList();
        loadIngredientsList();
        ingredientsListAdapter = new IngredientsListAdapter(this.getActivity(), R.layout.listview_item_ingredient, this.ingredients);

        spinnerRecipes = (Spinner) view.findViewById(R.id.spinner_recipes);
        recipesSpinnerAdapter = new RecipeSpinnerAdapter(getActivity(), android.R.layout.simple_list_item_1, this.recipes);
        spinnerRecipes.setAdapter(recipesSpinnerAdapter);


        spinnerType = (Spinner) view.findViewById(R.id.spinner_type);
        ArrayAdapter<CharSequence> adapterType = ArrayAdapter.createFromResource(this.getActivity(),
                R.array.recipe_type, android.R.layout.simple_spinner_item);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType.setAdapter(adapterType);

        etRecipeName = (EditText) view.findViewById(R.id.et_recipe_name);
        etRecipeDescription = (EditText) view.findViewById(R.id.et_recipe_description);
        tvIngredientsDescription = (TextView) view.findViewById(R.id.tv_ingredients_description);


        btnPickIngredients = (Button) view.findViewById(R.id.btn_pick_ingredients);
        btnPickIngredients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showListDialog("Wybierz składniki", "Anuluj");
            }
        });

        btnAdd = (Button) view.findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addRecipe();

            }
        });

        btnSave = (Button) view.findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveRecipe();
            }
        });

        btnDelete = (Button) view.findViewById(R.id.btn_delete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteRecipe();
            }
        });

        spinnerRecipes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Recipe recipe = recipes.get(position);
                if (recipe != null && recipe != nullRecipe) {
                    etRecipeName.setText(recipe.getName());
                    etRecipeDescription.setText(recipe.getDescription());
                    spinnerType.setSelection(recipe.getType());

                } else {
                    etRecipeName.setText("");
                    etRecipeDescription.setText("");
                    spinnerType.setSelection(0);
                }
                loadRecipeIngredientsList(recipe != null ? recipe.getId() : -1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        fromValidationInitialization();

        return view;
    }

    private void fromValidationInitialization() {
        mForm = new Form(this.getActivity());

        mForm.setValidationFailedRenderer(new CroutonValidationFailedRenderer(this.getActivity()));
        mForm.addField(Field.using(etRecipeName).validate(NotEmpty.build(this.getActivity(),"Nazwa przepisu")));
        mForm.addField(Field.using(etRecipeDescription).validate(NotEmpty.build(this.getActivity(),"Opis przepisu")));
    }

    private void saveRecipe() {
        if (spinnerRecipes.getSelectedItemPosition() == 0) {
            return;
        }
        Recipe recipe = (Recipe) spinnerRecipes.getSelectedItem();
        if (recipe != null && recipe != nullRecipe) {
            recipe.setName(etRecipeName.getText().toString());
            recipe.setDescription(etRecipeDescription.getText().toString());
            recipe.setType(spinnerType.getSelectedItemPosition());
            try {
                dbHelper.getRecipeDao().createOrUpdate(recipe);
            } catch (SQLException e) {
                e.printStackTrace();
                //TODO: error dialog
                return;
            }

            saveRecipeIngredients(recipe,MODE_SAVE_UPDATE);

            loadRecipesList();
            loadIngredientsDescription();
//            loadRecipeIngredientsList(recipe != null ? recipe.getId() : -1);
        }
    }

    private void addRecipe() {
        if (mForm.isValid()) {
            Crouton.makeText(getActivity(), "Form is valid", Style.CONFIRM).show();
            String name = etRecipeName.getText().toString();
            String description = etRecipeDescription.getText().toString();
            int type = spinnerType.getSelectedItemPosition();
            Recipe recipe = new Recipe(name, description, type);
            try {
                dbHelper.getRecipeDao().create(recipe);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            saveRecipeIngredients(recipe,MODE_SAVE_ADD);
            loadRecipesList();
            loadIngredientsDescription();
            if(recipe!=null){
//                        loadRecipeIngredientsList(recipe.getId());
//                        spinnerRecipes.setSelection(0);
                //setSpinnerAtRecipe(0);
            }else{
//                        loadRecipeIngredientsList(-1);
            }


        }
    }

    private void deleteRecipe() {
        if (spinnerRecipes.getSelectedItemPosition() == 0) {
            return;
        }
        Recipe recipe = (Recipe) spinnerRecipes.getSelectedItem();
        if (recipe != null && recipe != nullRecipe) {
            try {
                int recipeId = recipe.getId();
                dbHelper.getRecipeDao().delete(recipe);
                DeleteBuilder<RecipeIngredient, Integer> deleteBuilder = dbHelper.getRecipeIngredientDao().deleteBuilder();
                deleteBuilder.where().eq("idRecipe", recipeId);
                deleteBuilder.delete();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            loadRecipesList();
            spinnerRecipes.setSelection(0);
        }
    }


    private void saveRecipeIngredients(Recipe recipe, int mode) {
        LinkedList<RecipeIngredient> itemsToDelete = new LinkedList<>();
        LinkedList<RecipeIngredient> itemsCreate = new LinkedList<>();
        LinkedList<RecipeIngredient> itemsUpdate = new LinkedList<>();
        for (Ingredient ingredient : this.ingredients) {
            if(mode == MODE_SAVE_ADD){
               ingredient.setDirty(true);
            }
            if (ingredient.isPicked()) {
                if (ingredient.isDirty()) {
                    ingredient.getRecipeIngredient().setIdRecipe(recipe.getId());
                    itemsCreate.add(ingredient.getRecipeIngredient());
                } else if (ingredient.getRecipeIngredient().isModified()) {
                    itemsUpdate.add(ingredient.getRecipeIngredient());
                }
            } else {
                if (!ingredient.isDirty()) {
                    itemsToDelete.add(ingredient.getRecipeIngredient());
                }
            }
            ingredient.setDirty(true);
        }
        if(this.ingredientsListAdapter!=null) {
            this.ingredientsListAdapter.notifyDataSetChanged();
        }
        try {

            for (RecipeIngredient ri : itemsToDelete) {
                dbHelper.getRecipeIngredientDao().delete(ri);
            }
            for (RecipeIngredient ri : itemsUpdate) {
                dbHelper.getRecipeIngredientDao().update(ri);
            }
            for (RecipeIngredient ri : itemsCreate) {
                dbHelper.getRecipeIngredientDao().create(ri);
            }
        } catch (SQLException e) {
            //TODO: error dialog
            e.printStackTrace();
        }
    }

    private void loadRecipesList() {
        try {
            if (this.recipes == null) {
                this.recipes = new LinkedList<>();
            } else {
                this.recipes.clear();
            }
            this.recipes.add(this.nullRecipe);
            this.recipes.addAll(dbHelper.getRecipeDao().queryForAll());
        } catch (SQLException e) {
            this.recipes = new LinkedList<>();
            e.printStackTrace();
        }
        if (this.recipesSpinnerAdapter != null) {
            this.recipesSpinnerAdapter.notifyDataSetChanged();
        }
    }

    private void loadIngredientsList() {
        try {
            this.ingredients = dbHelper.getIngredientDao().queryForAll();
        } catch (SQLException e) {
            this.ingredients = new LinkedList<>();
            e.printStackTrace();
        }
        if (this.ingredientsListAdapter != null) {
            this.ingredientsListAdapter.notifyDataSetChanged();
        }
    }

    private void loadRecipeIngredientsList(int recipeId) {
        if (recipeId > 0) {
            try {
                this.recipeIngredientList = dbHelper.getRecipeIngredientDao().queryForEq("idRecipe", recipeId);
            } catch (SQLException e) {
                this.recipeIngredientList = new LinkedList<>();
                e.printStackTrace();
            }
        } else {
            this.recipeIngredientList = new LinkedList<>();
        }

        for (Ingredient i : this.ingredients) {
            boolean contained = false;
            for (RecipeIngredient ri : this.recipeIngredientList) {
                if (i.getId() == ri.getIdIngredient()) {
                    i.setRecipeIngredient(ri);
                    contained = true;
                    break;
                }
            }
            if (!contained) {
                //RecipeIngredient dirtyRecipeIngredient = new RecipeIngredient(-1, i.getId(), 0);
                //dirtyRecipeIngredient.setDirty(true);
                i.setRecipeIngredient(null);
            }
        }
        if (this.ingredientsListAdapter != null) {
            this.ingredientsListAdapter.notifyDataSetChanged();
        }

        loadIngredientsDescription();
    }

    private void loadIngredientsDescription() {
        StringBuilder sb = new StringBuilder();
        for (Ingredient ingredient : this.ingredients) {
            if (ingredient.isPicked()) {
                sb.append(ingredient.getIngredientDescription(this.getActivity())).append("\n");
            }
        }
        tvIngredientsDescription.setText(sb.toString());
    }

    private void showListDialog(String title, String negativeText) {

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        IngredientsDialogFragment newFragment = IngredientsDialogFragment.newInstance(this, title);
        newFragment.show(ft, "dialog");
    }

    @Override
    public IngredientsListAdapter getIngredientsAdapter() {
        return this.ingredientsListAdapter;
    }
}
