package pl.wildeastcoders.dieta.utils;

import android.app.Activity;

import java.util.ArrayList;
import java.util.List;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import ua.org.zasadnyy.zvalidations.ValidationFailedRenderer;
import ua.org.zasadnyy.zvalidations.ValidationResult;

/**
 * Created by PR on 2015-02-09.
 */

public class CroutonValidationFailedRenderer implements ValidationFailedRenderer {

    private Activity mActivity;
    private List<Crouton> mCroutons = new ArrayList<Crouton>();

    public CroutonValidationFailedRenderer(Activity activity) {
        this.mActivity = activity;
    }

    @Override
    public void showErrorMessage(ValidationResult validationResult) {
        Crouton crouton = Crouton.makeText(mActivity, validationResult.getMessage(), Style.ALERT);
        mCroutons.add(crouton);
        crouton.show();
    }

    @Override
    public void clear() {
        for (Crouton crouton : mCroutons) {
            crouton.cancel();
        }
        mCroutons.clear();
    }
}
