package pl.wildeastcoders.dieta.utils;

import android.content.Context;

import java.util.List;

import pl.wildeastcoders.dieta.database.model.FoodWeek;
import pl.wildeastcoders.dieta.database.model.MultiIngredient;

/**
 * Created by PR on 2015-02-26.
 */
public class ShopingListGenerator {
    private FoodWeek foodWeek;
    private Context context;

    public ShopingListGenerator(FoodWeek foodWeek, Context context) {
        this.foodWeek = foodWeek;
        this.context = context;
    }

    public static String multiIngredientsToString(List<MultiIngredient> multiIngredients, Context context) {
        StringBuilder stringBuilder = new StringBuilder();
        for (MultiIngredient mi : multiIngredients) {
            stringBuilder.append(mi.readIngredientString(context));
        }
        return stringBuilder.toString();
    }

    public boolean isShopingListGeneratorReady() {
        return this.foodWeek != null;
    }

    public String generateListString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(foodWeek.getWeekDescription(context));
        return stringBuilder.toString();
    }
}
