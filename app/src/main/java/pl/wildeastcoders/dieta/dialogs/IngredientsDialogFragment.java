package pl.wildeastcoders.dieta.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import pl.wildeastcoders.dieta.R;
import pl.wildeastcoders.dieta.adapters.IngredientsListAdapter;

/**
 * Created by PR on 2015-02-05.
 */
public class IngredientsDialogFragment extends DialogFragment {

    public interface IngredientsModificationListener{
        public IngredientsListAdapter getIngredientsAdapter();
    }

    private IngredientsModificationListener listener;

    public static IngredientsDialogFragment newInstance(IngredientsModificationListener listener, String title) {
        IngredientsDialogFragment f = new IngredientsDialogFragment();
        f.setIngredientsModificationListener(listener);
        return f;
    }

    public void setIngredientsModificationListener(IngredientsModificationListener listener){
        this.listener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_fragment_ingredients_list, container, false);
        ListView lvIngredients = (ListView)v.findViewById(R.id.lv_ingredients);
        Button btnClose = (Button) v.findViewById(R.id.btn_close);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        if(this.listener!=null){
//            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this.getActivity(),R.layout.listview_item_ingredient, new String[]{"A","B","C"});

            lvIngredients.setAdapter(this.listener.getIngredientsAdapter());
//            lvIngredients.setAdapter(arrayAdapter);
        }
        lvIngredients.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("", "");
            }
        });

        return v;
    }
}
