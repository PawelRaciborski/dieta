package pl.wildeastcoders.dieta;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import pl.wildeastcoders.dieta.database.DatabaseHelper;


/**
 * An activity representing a list of Pozycje. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link PozycjaDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 * <p/>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link PozycjaListFragment} and the item details
 * (if present) is a {@link PozycjaDetailFragment}.
 * <p/>
 * This activity also implements the required
 * {@link PozycjaListFragment.Callbacks} interface
 * to listen for item selections.
 */
public class PozycjaListActivity extends FragmentActivity
        implements PozycjaListFragment.Callbacks {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    private DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pozycja_list);

        if (findViewById(R.id.pozycja_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;

            // In two-pane mode, list items should be given the
            // 'activated' state when touched.
            ((PozycjaListFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.pozycja_list))
                    .setActivateOnItemClick(true);
        }

        // TODO: If exposing deep links into your app, handle intents here.
    }


    /**
     * Callback method from {@link PozycjaListFragment.Callbacks}
     * indicating that the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(String id) {
        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putString(PozycjaDetailFragment.ARG_ITEM_ID, id);
            BaseDbFragment fragment;

            switch (id) {
                case "1":
                    fragment = new AddIngredientFragment();
                    break;
                case "2":
                    fragment = new AddRecipeFragment();
                    break;
                case "3":
                    fragment = new GenerateShopingListFragment();
                    break;
                default:
                    return;
            }

            fragment.setDbHelper(dbHelper);
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction().replace(R.id.pozycja_detail_container, fragment).commit();

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent detailIntent = new Intent(this, PozycjaDetailActivity.class);
            detailIntent.putExtra(PozycjaDetailFragment.ARG_ITEM_ID, id);
            startActivity(detailIntent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mTwoPane) {
            dbHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
    }

    @Override
    protected void onPause() {
        if (mTwoPane) {
            OpenHelperManager.releaseHelper();
        }
        super.onPause();
    }
}
